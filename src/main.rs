use aes::Aes256;
use block_modes::block_padding::ZeroPadding;
use block_modes::{BlockMode, Cbc};
use p256::{ecdh::EphemeralSecret, EncodedPoint, PublicKey};
use rand::prelude::*;
use rand_core::OsRng; // requires 'getrandom' feature
use sha2::{Digest, Sha256};
use std::fs;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::str;

type Aes256Cbc = Cbc<Aes256, ZeroPadding>;

fn handle(mut stream: TcpStream) {
    let client_addr = stream.peer_addr().ok().unwrap();
    println!("Connection accepted from {}", client_addr);

    let mut buf = [0; 6];
    stream.read_exact(&mut buf).ok();
    if &buf != b"START\n" {
        eprintln!("Client didn't initialize properly! Ending connection");
        return;
    }
    let alice_secret = EphemeralSecret::random(&mut OsRng);
    let alice_pk_bytes = EncodedPoint::from(alice_secret.public_key());
    //print_long_hex("SERVER PK = ", &alice_pk_bytes.to_bytes()[1..]);
    //println!("SERVER PK = {:?}", &alice_pk_bytes.to_bytes()[1..]);
    stream.write_all(&alice_pk_bytes.to_bytes()[1..]).unwrap();
    let mut buf = [0; 64];
    stream.read_exact(&mut buf).ok();
    //print_long_hex("CLIENT PK = ", &buf);
    //println!("CLIENT PK = {:?}", &buf);
    let mut client_public = [4; 65];
    for i in 0..64 {
        client_public[i + 1] = buf[i];
    }
    let client_public = PublicKey::from_sec1_bytes(&client_public).unwrap();
    let shared = alice_secret.diffie_hellman(&client_public);
    //println!("Shared key = {:?}", shared.as_bytes());
    //print_long_hex("Shared key = ", shared.as_bytes().as_slice());

    let mut hasher = Sha256::new();
    hasher.update(shared.as_bytes().as_slice());
    let result = hasher.finalize();
    //print_long_hex("Hash = ", &result[..]);

    // https://rust-random.github.io/rand/rand/rngs/struct.ThreadRng.html
    // ThreadRng uses the same PRNG as StdRng for security and performance and is automatically seeded from OsRng
    let mut rng = rand::thread_rng();
    let iv = rng.gen::<[u8; 16]>();
    //print_long_hex("IV = ", &iv);
    stream.write_all(&iv).ok().unwrap();

    loop {
        //print_long_hex("Raw command: ", command);
        let mut buf = [0; 64];
        if let Err(error) = stream.read_exact(&mut buf[..]) {
            eprintln!("Error: {}. Disconnecting from client...", error);
            return;
        };
        //print_long_hex("Incoming data: ", &buf[..]);
        let cipher = Aes256Cbc::new_var(&result[..], &iv).unwrap();
        let command = cipher.decrypt(&mut buf).unwrap();
        if let Ok(command) = std::str::from_utf8(&command) {
            let mut parts = command.split_ascii_whitespace();
            let verb = parts.next();
            let arg = parts.next();
            match verb {
                Some("GET") => send_file(&mut stream, &result[..], &iv, arg.unwrap()),
                Some("PUT") => recv_file(
                    &mut stream,
                    &result[..],
                    &iv,
                    arg.unwrap(),
                    parts.next().unwrap(),
                ),
                Some("LIST") => list_files(&mut stream, &result[..], &iv),
                Some("HASH") => send_hash(&mut stream, &result[..], &iv, arg.unwrap()),
                _ => {
                    eprintln!("Command {} not valid", command);
                    return;
                }
            }
        } else {
            println!("Ending connection to {}", client_addr);
            return;
        }
    }
}

#[allow(dead_code)]
fn print_long_hex(message: &str, arr: &[u8]) {
    print!("{}", message);
    for ch in arr {
        print!("{:02x}", ch);
    }
    println!("");
}

fn list_files(stream: &mut TcpStream, key: &[u8], iv: &[u8]) {
    println!(
        "Client {} requested list of files",
        stream.peer_addr().unwrap()
    );

    let files = fs::read_dir(".")
        .expect("Unable to read directory metadata")
        .filter_map(|f| f.ok())
        .filter(|f| !f.metadata().unwrap().is_dir())
        .map(|f| f.file_name().into_string().unwrap())
        .collect::<Vec<String>>();

    _encrypt_and_send_str(stream, key, iv, &files.len().to_string());

    for file in &files {
        let mut buf = [0; 64];
        buf[..file.len()].copy_from_slice(file.as_bytes());
        _encrypt_and_send(stream, key, iv, &mut buf);
    }
}

fn send_file(stream: &mut TcpStream, key: &[u8], iv: &[u8], filename: &str) {
    println!(
        "Client {} requested file {}",
        stream.peer_addr().unwrap(),
        filename
    );

    match fs::File::open(filename) {
        Ok(mut file) => {
            // Send file size to client
            _encrypt_and_send_str(stream, key, iv, &file.metadata().unwrap().len().to_string());

            loop {
                let mut buf = [0; 64];
                if let Ok(n) = file.read(&mut buf) {
                    /*
                    If n is 0, then it can indicate one of two scenarios:
                    1. This reader has reached its "end of file" and will likely no longer be able to produce bytes.
                    2. The buffer specified was 0 bytes in length.
                    */
                    if n == 0 {
                        break; // Finished reading file, break out of loop
                    }
                    _encrypt_and_send(stream, key, iv, &mut buf);
                }
            }
        }
        Err(error) => {
            eprintln!("Error {:?} while reading file {}", error.kind(), filename);
            _encrypt_and_send_str(stream, key, iv, "0");
        }
    }
}

fn recv_file(stream: &mut TcpStream, key: &[u8], iv: &[u8], filename: &str, filesize: &str) {
    let filesize = str::parse::<usize>(filesize).unwrap();
    println!(
        "Client {} uploaded file {} of {} bytes",
        stream.peer_addr().unwrap(),
        filename,
        filesize
    );

    match fs::File::create(filename) {
        Ok(mut file) => {
            let mut received = 0;
            loop {
                let mut buf = [0; 64];
                if let Ok(n) = stream.read(&mut buf) {
                    if n == 0 {
                        break; 
                    }
                    let cipher = Aes256Cbc::new_var(key, iv).unwrap();
                    cipher.decrypt(&mut buf).unwrap();

                    let n = if (filesize - received) >= 64 {
                        64
                    } else {
                        filesize - received
                    };
                    received += file.write(&mut buf[..n]).unwrap();
                }
            }
        }
        Err(error) => eprintln!(
            "Error {:?} while writing to file {}",
            error.kind(),
            filename
        ),
    }
}

fn send_hash(stream: &mut TcpStream, key: &[u8], iv: &[u8], filename: &str) {
    println!(
        "Client {} requested hash of file {}",
        stream.peer_addr().unwrap(),
        filename
    );

    match fs::File::open(filename) {
        Ok(mut file) => {
            let mut hasher = Sha256::new();
            loop {
                let mut buf = [0; 32];
                if let Ok(n) = file.read(&mut buf) {
                    if n == 0 {
                        break;
                    }
                    hasher.update(&buf[..n]);
                }
            }
            let result = hasher.finalize();
            let mut buf = [0; 64];
            buf[..32].copy_from_slice(&result[..]);
            _encrypt_and_send(stream, key, iv, &mut buf[..]);
        }
        Err(error) => {
            eprintln!("Error {:?} while hashing file {}", error.kind(), filename);
            _encrypt_and_send_str(stream, key, iv, ""); // Send string full of nulls
        }
    }
}

fn _encrypt_and_send_str(stream: &mut TcpStream, key: &[u8], iv: &[u8], text: &str) {
    let mut buf = vec![0u8; 64];
    for (n, char) in text.as_bytes().iter().enumerate() {
        buf[n] = *char;
    }
    _encrypt_and_send(stream, key, iv, &mut buf);
}

fn _encrypt_and_send(stream: &mut TcpStream, key: &[u8], iv: &[u8], buf: &mut [u8]) {
    let cipher = Aes256Cbc::new_var(key, iv).unwrap();
    cipher.encrypt(buf, 64).unwrap();
    stream.write_all(&buf).unwrap();
}

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind(("localhost", 8080))?;

    for conn in listener.incoming() {
        std::thread::spawn(move || {
            handle(conn.unwrap());
        });
    }
    Ok(())
}
