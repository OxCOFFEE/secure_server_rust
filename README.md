# secure_server

A reimplementation of the [secure_server](https://bitbucket.org/OxCOFFEE/secure_server/src/master/) server app in pure Rust.

The server can interoperate with the original C client.However, to use it, [line 60](https://bitbucket.org/OxCOFFEE/secure_server/src/08564832b92b94169b9925378896133128c6a813/client.c#lines-60) of `client.c` must be changed so that the client uses `uECC_secp256r1` instead of `uECC_secp160r1`, since there appears to be no implementation of `secp160r1` in Rust, and `secp256` is more secure anyways. The client must be then recompiled (see [here](https://bitbucket.org/OxCOFFEE/secure_server/src/master/#markdown-header-compiling) for instructions).